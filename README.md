# About
This is docker container based on *AlmaLinux* with enabled man pages

# Usage
```console
./build        # build image
./build --push # build image and push to dockerhub
./run          # run container
```
