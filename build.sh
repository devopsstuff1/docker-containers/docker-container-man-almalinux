#!/bin/sh

p=$(realpath "$0")
d=$(dirname "$p")

cd "$d" || exit 1

docker-compose build

if [ "$1" = "--push" ]; then
    docker push -a asddsajpg/man_almalinux
fi
