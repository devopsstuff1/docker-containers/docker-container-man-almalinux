FROM almalinux:8

RUN dnf install -y man man-pages man-db

RUN rpm -qa | xargs dnf -y reinstall
